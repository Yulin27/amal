import logging

from torch.nn.modules.pooling import MaxPool1d
logging.basicConfig(level=logging.INFO)

import heapq
from pathlib import Path
import gzip

from tqdm import tqdm

import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
import sentencepiece as spm

from tp8_preprocess import TextDataset
from utils import EarlyStopper, train_loop, test_loop, predict
# Utiliser tp8_preprocess pour générer le vocabulaire BPE et
# le jeu de donnée dans un format compact

# --- Configuration

# Taille du vocabulaire
vocab_size = 1000
MAINDIR = Path(__file__).parent

# Chargement du tokenizer

tokenizer = spm.SentencePieceProcessor()
tokenizer.Load(f"wp{vocab_size}.model")
ntokens = len(tokenizer)

def loaddata(mode):
    with gzip.open(f"{mode}-{vocab_size}.pth", "rb") as fp:
        return torch.load(fp)


test = loaddata("test")
train = loaddata("train")
TRAIN_BATCHSIZE=500
TEST_BATCHSIZE=500


# --- Chargements des jeux de données train, validation et test

# data = [data for data, _ in train]
# max_length = max([len(d) for d in data])


val_size = 1000
train_size = len(train) - val_size
train, val = torch.utils.data.random_split(train, [train_size, val_size])

logging.info("Datasets: train=%d, val=%d, test=%d", train_size, val_size, len(test))
logging.info("Vocabulary size: %d", vocab_size)
train_iter = torch.utils.data.DataLoader(train, batch_size=TRAIN_BATCHSIZE, collate_fn=TextDataset.collate)
val_iter = torch.utils.data.DataLoader(val, batch_size=TEST_BATCHSIZE, collate_fn=TextDataset.collate)
test_iter = torch.utils.data.DataLoader(test, batch_size=TEST_BATCHSIZE, collate_fn=TextDataset.collate)


#  TODO: 





class cnn(nn.Module):
    def __init__(self):
        super().__init__()
        self.embedding = nn.Embedding(ntokens, 64)
        self.conv1 = nn.Conv1d(64, 128, 3, padding=1)
        self.conv2 = nn.Conv1d(128, 256, 3, padding=1)
        self.conv3 = nn.Conv1d(256, 512, 3, padding=1)
        self.conv4 = nn.Conv1d(512, 512, 3, padding=1)
        self.pool = nn.MaxPool1d(4)
        self.pool2 = nn.MaxPool1d(2)
        self.fc1 = nn.Linear(512, 256)
        self.fc2 = nn.Linear(256, 1)
        self.dropout = nn.Dropout(0.5)
        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        x = self.embedding(x)
        x = x.permute(0, 2, 1)
        x = self.conv1(x)
        x = self.relu(x)
        x = self.pool(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.pool(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.pool(x)
        x = self.conv4(x)
        x = self.relu(x)
        x = self.pool2(x)
        x = x.permute(0, 2, 1)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.fc2(x)
        x = self.sigmoid(x)
        return x
    
    

model = cnn()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=0.00001)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', patience=8, factor=0.5, verbose=True)
early_stopper = EarlyStopper(patience=15, min_delta=0.0001)

loss_fn = nn.BCELoss()

# --- Boucle d'apprentissage

writer = SummaryWriter()

for epoch in tqdm(range(10)):
    
    print(f"Epoch {epoch+1:>3d}",end=" ")
    train_loss = train_loop(train_iter, model, loss_fn, optimizer, shortcut=0)
    print(f"Train: {train_loss:>5f}", end=" ")
    val_loss = test_loop(val_iter, model, loss_fn)
    print(f"| Test: {val_loss:>5f}")

    if early_stopper.early_stop(val_iter, model):  
        model = early_stopper.get_best_model()
        break

    writer.add_scalar("Loss/train", train_loss, epoch)
    writer.add_scalar("Loss/test", val_loss, epoch)

# --- Évaluation finale
test_loss = test_loop(test_iter, model, loss_fn)
print(f"Test loss: {test_loss:>5f}")

