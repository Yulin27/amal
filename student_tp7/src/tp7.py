import logging
logging.basicConfig(level=logging.INFO)
import numpy as np
import os
from pathlib import Path
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, Dataset, random_split
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
import click

from datamaestro import prepare_dataset



# Ratio du jeu de train à utiliser
TRAIN_RATIO = 0.05
BATCH_SIZE = 300
LOG_PATH = "/tmp/runs/logs"


def store_grad(var):
    """Stores the gradient during backward

    For a tensor x, call `store_grad(x)`
    before `loss.backward`. The gradient will be available
    as `x.grad`

    """
    def hook(grad):
        var.grad = grad
    var.register_hook(hook)
    return var


#  TODO:  Implémenter

ds = prepare_dataset("com.lecun.mnist")
# Ne pas oublier se sous−échantillonner !
train_img , train_labels = ds.train.images.data() , ds.train.labels.data() 
test_img , test_labels = ds.test.images.data() , ds.test.labels.data()

train_img = train_img[:int(len(train_img)*TRAIN_RATIO)]
train_labels = train_labels[:int(len(train_labels)*TRAIN_RATIO)]

writter = SummaryWriter()

class MonDataset(Dataset):
    def __init__(self, X, Y) -> None:
        super().__init__()
        self.X = X.reshape(-1, 784).astype(np.float32) / 255
        self.Y = Y

    def __getitem__(self, index):
        return self.X[index], self.Y[index]
    
    def __len__(self):
        return len(self.X)
    
train_loader = DataLoader(MonDataset(train_img, train_labels), shuffle=True, batch_size=BATCH_SIZE)
test_loader = DataLoader(MonDataset(test_img, test_labels), shuffle=True, batch_size=BATCH_SIZE)



class model(nn.Module):
    def __init__(self,input_dim, hidden_dim, output_dim, dropout):
        super(model, self).__init__()

        # self.norm = nn.LayerNorm(input_dim)
        self.norm = nn.BatchNorm1d(input_dim)
        self.linear1 = nn.Linear(input_dim, hidden_dim)
        self.relu1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)
        self.linear2 = nn.Linear(hidden_dim, hidden_dim)
        self.relu2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        self.linear3 = nn.Linear(hidden_dim, hidden_dim)
        self.relu3 = nn.ReLU()
        self.dropout3 = nn.Dropout(dropout)
        self.classifier = nn.Linear(hidden_dim, output_dim)



    def forward(self, x):
        x = x.view(-1, 784)
        x = self.norm(x)
        x = self.linear1(x)
        x = self.relu1(x)
        x = self.dropout1(x)
        x = self.linear2(x)
        x = self.relu2(x)
        x = self.dropout2(x)
        x = self.linear3(x)
        x = self.relu3(x)
        x = self.dropout3(x)
        x = self.classifier(x)
        return x
    

def train(model, optimizer, train_loader, test_loader, device, epochs=1000):
    """Train a model on MNIST using the given optimizer and data loader"""
    criterion = nn.CrossEntropyLoss()
    for epoch in tqdm(range(epochs)):
        model.train()
        train_loss = 0.
        correct_train = 0.
        for batch_idx, (data, target) in enumerate(train_loader):
    
            data, target = data.to(torch.float).to(device), target.to(device)

            optimizer.zero_grad()

            target_pred = model(data)
            loss = criterion(target_pred, target)



            train_loss += loss.item()
            correct_train += (target_pred.argmax(dim=1) == target).sum().item()

            loss.backward()
            optimizer.step()

        model.eval()
        test_loss = 0.
        correct = 0.
        with torch.no_grad():
            for data, target in test_loader:
                data, target = data.to(device), target.to(device)

                target_pred = model(data)
                test_loss += criterion(target_pred, target).item()
                pred = target_pred.argmax(dim=1, keepdim=True)
                correct += pred.eq(target.view_as(pred)).sum().item()


        train_loss /= len(train_loader.dataset)
        train_accuracy = 100. * correct_train / len(train_loader.dataset)
        test_loss /= len(test_loader.dataset)
        test_accuracy = 100. * correct / len(test_loader.dataset)
        writter.add_scalar("train_loss", train_loss, epoch)
        writter.add_scalar("test_loss", test_loss, epoch)
        writter.add_scalar("train_accuracy", train_accuracy, epoch)
        writter.add_scalar("test_accuracy", test_accuracy, epoch)

        writter.add_histogram("model/linear1", model.linear1.weight, epoch)
        writter.add_histogram("model/linear2", model.linear2.weight, epoch)
        writter.add_histogram("model/linear3", model.linear3.weight, epoch)
        writter.add_histogram("model/classifier", model.classifier.weight, epoch)

        writter.add_histogram("model/linear1_grad", model.linear1.weight.grad, epoch)
        writter.add_histogram("model/linear2_grad", model.linear2.weight.grad, epoch)
        writter.add_histogram("model/linear3_grad", model.linear3.weight.grad, epoch)
        writter.add_histogram("model/classifier_grad", model.classifier.weight.grad, epoch)


        # save entropy sur la sortie du modèle
        entropy = -(F.softmax(target_pred, dim=1) * F.log_softmax(target_pred, dim=1))
        writter.add_histogram("entropy", entropy, epoch)



        print(f"Epoch {epoch}: train loss {train_loss:.4f} test loss {test_loss:.4f} train accuracy {train_accuracy:.2f} test accuracy {test_accuracy:.2f}")

model = model(784, 100, 10, 0.3)
# weight_decat = L2
optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=0.01)
train(model, optimizer, train_loader, test_loader, device="cpu", epochs=100)