import itertools
import logging
from tqdm import tqdm

from datamaestro import prepare_dataset
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import torch
from typing import List
import time
logging.basicConfig(level=logging.INFO)

ds = prepare_dataset('org.universaldependencies.french.gsd')

writer = SummaryWriter('runs/tp6-tagging')

# Format de sortie décrit dans
# https://pypi.org/project/conllu/

class Vocabulary:
    """Permet de gérer un vocabulaire.

    En test, il est possible qu'un mot ne soit pas dans le
    vocabulaire : dans ce cas le token "__OOV__" est utilisé.
    Attention : il faut tenir compte de cela lors de l'apprentissage !

    Utilisation:

    - en train, utiliser v.get("blah", adding=True) pour que le mot soit ajouté
      automatiquement s'il n'est pas connu
    - en test, utiliser v["blah"] pour récupérer l'ID du mot (ou l'ID de OOV)
    """
    OOVID = 1
    PAD = 0

    def __init__(self, oov: bool):
        """ oov : autorise ou non les mots OOV """
        self.oov =  oov
        self.id2word = [ "PAD"]
        self.word2id = { "PAD" : Vocabulary.PAD}
        if oov:
            self.word2id["__OOV__"] = Vocabulary.OOVID
            self.id2word.append("__OOV__")

    def __getitem__(self, word: str):
        if self.oov:
            return self.word2id.get(word, Vocabulary.OOVID)
        return self.word2id[word]

    def get(self, word: str, adding=True):
        try:
            return self.word2id[word]
        except KeyError:
            if adding:
                wordid = len(self.id2word)
                self.word2id[word] = wordid
                self.id2word.append(word)
                return wordid
            if self.oov:
                return Vocabulary.OOVID
            raise

    def __len__(self):
        return len(self.id2word)

    def getword(self,idx: int):
        if idx < len(self):
            return self.id2word[idx]
        return None

    def getwords(self,idx: List[int]):
        return [self.getword(i) for i in idx]



class TaggingDataset():
    def __init__(self, data, words: Vocabulary, tags: Vocabulary, adding=True):
        self.sentences = []

        for s in data:
            self.sentences.append(([words.get(token["form"], adding) for token in s], [tags.get(token["upostag"], adding) for token in s]))
    def __len__(self):
        return len(self.sentences)
    def __getitem__(self, ix):
        return self.sentences[ix]



def collate_fn(batch):
    """Collate using pad_sequence"""
    return tuple(pad_sequence([torch.tensor(b[j], dtype=torch.float) for b in batch]) for j in range(2))


logging.info("Loading datasets...")
words = Vocabulary(True)
tags = Vocabulary(False)
train_data = TaggingDataset(ds.train, words, tags, True)
dev_data = TaggingDataset(ds.validation, words, tags, True)
test_data = TaggingDataset(ds.test, words, tags, False)

logging.info("Vocabulary size: %d", len(words))


BATCH_SIZE=32

train_loader = DataLoader(train_data, collate_fn=collate_fn, batch_size=BATCH_SIZE, shuffle=True)
dev_loader = DataLoader(dev_data, collate_fn=collate_fn, batch_size=BATCH_SIZE)
test_loader = DataLoader(test_data, collate_fn=collate_fn, batch_size=BATCH_SIZE)




#  TODO:  Implémenter le modèle et la boucle d'apprentissage (en utilisant les LSTMs de pytorch)
class LSTMTagger(nn.Module):
    def __init__(self, input_size, batch_size, hidden_dim, nb_layer):
        super(LSTMTagger, self).__init__()
        self.input_size = input_size
        self.hidden_dim = hidden_dim
        self.nb_layer = nb_layer
        self.batch_size = batch_size
        self.lstm = nn.LSTM(input_size,hidden_dim, nb_layer, batch_first=True)
        self.hidden2tag = nn.Linear(hidden_dim, len(tags))
    
    def forward(self, sentence):
        lstm_out, _ = self.lstm(sentence)
        tag_space = self.hidden2tag(lstm_out)
        tag_scores = nn.functional.log_softmax(tag_space, dim=1)
        return tag_scores
    
def one_hot(y, n_dims=None):
    batch, seq_len = y.shape
    y_tensor = torch.zeros(batch, seq_len, n_dims)
    for i in range(batch):
        for j in range(seq_len):
            y_tensor[i, j, int(y[i, j])] = 1.
    return y_tensor

def train(model, train_loader, dev_loader, optimizer, loss_fn, device, nb_epochs, writer, verbose=True):
    model.train()
    for epoch in range(nb_epochs):
        total_loss = 0
        for batch in tqdm(train_loader, desc=f"Epoch {epoch}"):
            
            x, y = batch
            x, y = x.T.to(device), y.T.to(device)
            x = x.unsqueeze(2)
            y_pred = model(x)
            y = one_hot(y, len(tags))
            loss = loss_fn(y_pred, y)
            loss.backward()
            optimizer.step()
            total_loss += loss.item()
            optimizer.zero_grad()
        writer.add_scalar("Loss/train", total_loss, epoch)
        if verbose:
            print(f"Epoch {epoch} : loss = {total_loss}")

        # Evaluation
        model.eval()
        total_loss = 0
        for batch in dev_loader:
            x, y = batch
            x, y = x.T.to(device), y.T.to(device)
            x = x.unsqueeze(2)
            y = one_hot(y, len(tags))
            y_pred = model(x)
            loss = loss_fn(y_pred, y)
            total_loss += loss.item()
        writer.add_scalar("Loss/dev", total_loss, epoch)
        if verbose:
            print(f"Epoch {epoch} : loss = {total_loss}")

batch_size = 32
nb_layer = 2
hidden_dim = 100
input_size = 1
model = LSTMTagger(1, batch_size, hidden_dim, nb_layer)
loss_fn = nn.functional.cross_entropy
optimizer = optim.Adam(model.parameters(), lr=0.01)
device = torch.device("cpu")

train(model, train_loader, dev_loader, optimizer, loss_fn, device, 10, writer, verbose=True)

model.eval()
total_loss = 0
for batch in test_loader:
    x, y = batch
    x, y = x.T.to(device), y.T.to(device)
    x = x.unsqueeze(2)
    y = one_hot(y, len(tags))
    y_pred = model(x)
    loss = loss_fn(y_pred, y)
    total_loss += loss.item()
writer.add_scalar("Loss/test", total_loss)
print(f"Test : loss = {total_loss}")