import logging
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import torch
import unicodedata
import string
from tqdm import tqdm
from pathlib import Path
from typing import List

import time
import re
import random
from torch.utils.tensorboard import SummaryWriter




logging.basicConfig(level=logging.INFO)

FILE = "data/en-fra.txt"

writer = SummaryWriter("/tmp/runs/tag-"+time.asctime())

def normalize(s):
    return re.sub(' +',' ', "".join(c if c in string.ascii_letters else " "
         for c in unicodedata.normalize('NFD', s.lower().strip())
         if  c in string.ascii_letters+" "+string.punctuation)).strip()


class Vocabulary:
    """Permet de gérer un vocabulaire.

    En test, il est possible qu'un mot ne soit pas dans le
    vocabulaire : dans ce cas le token "__OOV__" est utilisé.
    Attention : il faut tenir compte de cela lors de l'apprentissage !

    Utilisation:

    - en train, utiliser v.get("blah", adding=True) pour que le mot soit ajouté
      automatiquement
    - en test, utiliser v["blah"] pour récupérer l'ID du mot (ou l'ID de OOV)
    """
    PAD = 0
    EOS = 1
    SOS = 2
    OOVID = 3

    def __init__(self, oov: bool):
        self.oov = oov
        self.id2word = ["PAD", "EOS", "SOS"]
        self.word2id = {"PAD": Vocabulary.PAD, "EOS": Vocabulary.EOS, "SOS": Vocabulary.SOS}
        if oov:
            self.word2id["__OOV__"] = Vocabulary.OOVID
            self.id2word.append("__OOV__")

    def __getitem__(self, word: str):
        if self.oov:
            return self.word2id.get(word, Vocabulary.OOVID)
        return self.word2id[word]

    def get(self, word: str, adding=True):
        try:
            return self.word2id[word]
        except KeyError:
            if adding:
                wordid = len(self.id2word)
                self.word2id[word] = wordid
                self.id2word.append(word)
                return wordid
            if self.oov:
                return Vocabulary.OOVID
            raise

    def __len__(self):
        return len(self.id2word)

    def getword(self, idx: int):
        if idx < len(self):
            return self.id2word[idx]
        return None

    def getwords(self, idx: List[int]):
        return [self.getword(i) for i in idx]



class TradDataset():
    def __init__(self,data,vocOrig,vocDest,adding=True,max_len=10):
        self.sentences =[]
        for s in tqdm(data.split("\n")):
            if len(s)<1:continue
            orig,dest=map(normalize,s.split("\t")[:2])
            if len(orig)>max_len: continue
            self.sentences.append((torch.tensor([vocOrig.get(o) for o in orig.split(" ")]+[Vocabulary.EOS]),torch.tensor([vocDest.get(o) for o in dest.split(" ")]+[Vocabulary.EOS])))
    def __len__(self):return len(self.sentences)
    def __getitem__(self,i): return self.sentences[i]



def collate_fn(batch):
    orig,dest = zip(*batch)
    o_len = torch.tensor([len(o) for o in orig])
    d_len = torch.tensor([len(d) for d in dest])
    return pad_sequence(orig),o_len,pad_sequence(dest),d_len


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


with open(FILE) as f:
    lines = f.readlines()

lines = [lines[x] for x in torch.randperm(len(lines))]
idxTrain = int(0.8*len(lines))

vocEng = Vocabulary(True)
vocFra = Vocabulary(True)
MAX_LEN=100
BATCH_SIZE=100

datatrain = TradDataset("".join(lines[:idxTrain]),vocEng,vocFra,max_len=MAX_LEN)
datatest = TradDataset("".join(lines[idxTrain:]),vocEng,vocFra,max_len=MAX_LEN)

train_loader = DataLoader(datatrain, collate_fn=collate_fn, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(datatest, collate_fn=collate_fn, batch_size=BATCH_SIZE, shuffle=True)

#  TODO:  Implémenter l'encodeur, le décodeur et la boucle d'apprentissage
class encodeur(nn.Module):
    def __init__(self, input_size, hidden_size, n_layers=1, dropout=0.1):
        super(encodeur, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.dropout = dropout
        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, n_layers, dropout=self.dropout, bidirectional=True)

    def forward(self, input, hidden):

        embedded = self.embedding(input)
        output, hidden = self.gru(embedded, hidden)
        return output, hidden
    
    def initHidden(self, batch_size):
        return torch.zeros(self.n_layers*2, batch_size, self.hidden_size, device=device)
    
class decodeur(nn.Module):
    def __init__(self, hidden_size, output_size, n_layers=1, dropout=0.1):
        super(decodeur, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.n_layers = n_layers
        self.dropout = dropout
        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, n_layers, dropout=self.dropout, bidirectional=True)
        self.out = nn.Linear(hidden_size*2, output_size)
        self.softmax = nn.LogSoftmax(dim=1)
    
    def forward(self, input, hidden):
        output = self.embedding(input)
        output = nn.functional.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output))
        return output, hidden
    
    def initHidden(self, batch_size):
        return torch.zeros(self.n_layers*2, batch_size, self.hidden_size, device=device)
    
class Seq2Seq(nn.Module):
    def __init__(self, encodeur, decodeur, device):
        super(Seq2Seq, self).__init__()
        self.encodeur = encodeur
        self.decodeur = decodeur
        self.device = device
        
    def forward(self, input, target, teacher_forcing_ratio=0.5):
        batch_size = input.shape[1]
        target_len = target.shape[0]
        target_vocab_size = self.decodeur.output_size
        
        outputs = torch.zeros(target_len, batch_size, target_vocab_size).to(self.device)
        encodeur_output, hidden = self.encodeur(input, self.encodeur.initHidden(batch_size))
        decodeur_input = torch.tensor([[Vocabulary.SOS]]*batch_size, device=device).T
        
        for t in range(1, target_len):
            decodeur_output, hidden = self.decodeur(decodeur_input, hidden)
            outputs[t] = decodeur_output
            teacher_force = random.random() < teacher_forcing_ratio

            top1 = decodeur_output.argmax(2)

            decodeur_input = target[t].reshape(1, -1) if teacher_force else top1
        return outputs
    
    def predict(self, input, max_length=MAX_LEN):
        batch_size = input.shape[1]
        target_len = max_length
        target_vocab_size = self.decodeur.output_size
        
        outputs = torch.zeros(target_len, batch_size, target_vocab_size).to(self.device)
        encodeur_output, hidden = self.encodeur(input, self.encodeur.initHidden(batch_size))
        decodeur_input = torch.tensor([[Vocabulary.SOS]]*batch_size, device=device)
        
        for t in range(1, target_len):
            decodeur_output, hidden = self.decodeur(decodeur_input, hidden)
            outputs[t] = decodeur_output
            top1 = decodeur_output.argmax(1)
            decodeur_input = top1
        return outputs
    
    def predict_sentence(self, input, max_length=MAX_LEN):
        output = self.predict(input, max_length)
        output = output.argmax(1)
        return output
    
    def predict_sentence_str(self, input, max_length=MAX_LEN):
        output = self.predict_sentence(input, max_length)
        return " ".join([vocFra.getword(o.item()) for o in output[1:]])
  
def one_hot(y, n_dims=None):
    batch, seq_len = y.size()
    y_tensor = torch.zeros(batch, seq_len, n_dims)
    for i in range(batch):
        for j in range(seq_len):
            y_tensor[i, j, int(y[i, j])] = 1.
    return y_tensor

def train(model, train_loader, dev_loader, optimizer, loss_fn, device, nb_epochs, writer, verbose=True):
    
    for epoch in range(nb_epochs):
        model.train()
        total_loss = 0
        for batch in tqdm(train_loader, desc=f"Epoch {epoch}"):
            
            x, x_len, y, y_len = batch
            x, y = x.to(device), y.to(device)
            y_one_hot = one_hot(y, len(vocFra)).to(device)
            y_pred = model(x, y)
            loss = loss_fn(y_pred, y_one_hot)
            loss.backward()
            optimizer.step()
            total_loss += loss.item()
            optimizer.zero_grad()
        writer.add_scalar("Loss/train", total_loss, epoch)
        if verbose:
            print(f"Epoch {epoch} : loss = {total_loss}")

        # Evaluation
        model.eval()
        total_loss = 0
        for batch in dev_loader:
            x, x_len, y, y_len = batch
            x, y = x.to(device), y.to(device)
            y_one_hot = one_hot(y, len(vocFra)).to(device)
            y_pred = model(x, y)
            loss = loss_fn(y_pred, y_one_hot)

            total_loss += loss.item()
        writer.add_scalar("Loss/dev", total_loss, epoch)
        if verbose:
            print(f"Epoch {epoch} : loss = {total_loss}")


hidden_size = 32
n_layers = 2
dropout = 0.1
learning_rate = 0.01
nb_epochs = 10

encodeur = encodeur(len(vocEng), hidden_size, n_layers, dropout).to(device)
decodeur = decodeur(hidden_size, len(vocFra), n_layers, dropout).to(device)
model = Seq2Seq(encodeur, decodeur, device).to(device)
loss_fn = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

try:
    model.load_state_dict(torch.load("model.pth"))
    print("Model loaded")
except FileNotFoundError:
    train(model, train_loader, test_loader, optimizer, loss_fn, device, nb_epochs, writer, verbose=True)
    # save model
    torch.save(model.state_dict(), "model.pth")

import numpy as np
x = torch.tensor([[vocEng["i"], vocEng["am"], vocEng["a"], vocEng["student"], vocEng["."]]]).T.to(device)
print(x.size())
print(model.predict_sentence_str(x))

    
    