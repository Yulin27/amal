
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from textloader import *
from generate import *
import torch.nn.functional as F




def maskedCrossEntropy(output: torch.Tensor, target: torch.LongTensor, target_emb:torch.Tensor, padcar: int):

    # Créer un masque pour les caractères de padding
    mask = (target != padcar)
    loss = F.cross_entropy(output, target_emb, reduction='none')

    # Appliquer le masque sur la perte
    masked_loss = loss * (1-mask.type_as(loss))  # Assurez-vous que le masque est du même type que la perte

    # Calculer la perte moyenne en ignorant les valeurs de padding
    num_non_pad_tokens = mask.sum()
    total_loss = masked_loss.sum()
    average_loss = total_loss / num_non_pad_tokens

    return average_loss


def find_closest_indices(embeddings, embedding_matrix):
    closest_indices = []
    for embedding in embeddings:
        similarities = F.cosine_similarity(embedding.unsqueeze(0), embedding_matrix)
        closest_index = torch.argmax(similarities)
        closest_indices.append(closest_index.item())
    return closest_indices

def calculate_accuracy(predicted_indices, target_indices):
    correct_predictions = sum(p == t for p, t in zip(predicted_indices, target_indices))
    accuracy = correct_predictions / len(target_indices)
    return accuracy

class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, vocab_size=97):
        super().__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size= output_size
        
        # Ajout d'une couche d'embedding
        self.embedding = nn.Embedding(vocab_size, hidden_size)

        # Define learnable parameters: weights and bias
        self.wi = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.wh = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.bh = nn.Parameter(torch.zeros(1, hidden_size))
        self.fc = nn.Linear(hidden_size, output_size)
        self.tanh = nn.Tanh()
     
    def init_hidden(self, batch_size):
        self.h = torch.zeros(batch_size, self.hidden_size)

    def one_step(self, xi):
        return torch.tanh(torch.mm(xi, self.wi) + torch.mm(self.h, self.wh) + self.bh)

    def forward(self, x):
        batch_size = x.size()[0]
        seq_length = x.size()[1]
        self.init_hidden(batch_size)
        embedded = self.embedding(x)

        outputs = []

        for t in range(seq_length):
            xi = embedded[:, t, :]
            ht = self.one_step(xi)
            self.h = ht  # Update hidden state for the next step
            outputs.append(ht)


        

        return torch.stack(outputs, dim=1)
    
    def decode(self,h):
        y = self.fc(h)
        return y
    

    def embed(self, x):
        return self.embedding(x)
    
class LSTM(RNN):
    def __init__(self, input_size, hidden_size, output_size, vocab_size=97):
        super().__init__(input_size, hidden_size, output_size, vocab_size)
        self.wf = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.wg = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.wo = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.bf = nn.Parameter(torch.zeros(1, hidden_size))
        self.bg = nn.Parameter(torch.zeros(1, hidden_size))
        self.bo = nn.Parameter(torch.zeros(1, hidden_size))
        self.sigmoid = nn.Sigmoid()
        self.tanh = nn.Tanh()

    def one_step(self, xi):
        ft = self.sigmoid(torch.mm(xi, self.wf) + torch.mm(self.h, self.wh) + self.bf)
        gt = self.tanh(torch.mm(xi, self.wg) + torch.mm(self.h, self.wh) + self.bg)
        ot = self.sigmoid(torch.mm(xi, self.wo) + torch.mm(self.h, self.wh) + self.bo)
        ct = ft * self.c + gt
        ht = ot * self.tanh(ct)
        self.c = ct
        self.h = ht
        return ht
    
    def init_hidden(self, batch_size):
        self.h = torch.zeros(batch_size, self.hidden_size)
        self.c = torch.zeros(batch_size, self.hidden_size)


class GRU(RNN):
    def __init__(self, input_size, hidden_size, output_size, vocab_size=97):
        super().__init__(input_size, hidden_size, output_size, vocab_size)
        self.wz = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.wr = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.wg = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.bz = nn.Parameter(torch.zeros(1, hidden_size))
        self.br = nn.Parameter(torch.zeros(1, hidden_size))
        self.bg = nn.Parameter(torch.zeros(1, hidden_size))
        self.sigmoid = nn.Sigmoid()
        self.tanh = nn.Tanh()

    def one_step(self, xi):
        zt = self.sigmoid(torch.mm(xi, self.wz) + torch.mm(self.h, self.wh) + self.bz)
        rt = self.sigmoid(torch.mm(xi, self.wr) + torch.mm(self.h, self.wh) + self.br)
        gt = self.tanh(torch.mm(xi, self.wg) + torch.mm(rt * self.h, self.wh) + self.bg)
        ht = (1 - zt) * self.h + zt * gt
        self.h = ht
        return ht
    
    def init_hidden(self, batch_size):
        self.h = torch.zeros(batch_size, self.hidden_size)

def normalization(y):
    min_val = y.min(dim=1, keepdim=True)[0]
    max_val = y.max(dim=1, keepdim=True)[0]
    y_normalized = (y - min_val) / (max_val - min_val + 1e-5)
    return y_normalized

#  TODO:  Reprenez la boucle d'apprentissage, en utilisant des embeddings plutôt que du one-hot

if __name__ == "__main__":

    # Paramètres
    batch_size = 64
    hidden_size = 64
    learning_rate = 0.001
    epochs = 20
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print("Using device: ", device)

    # Chargement des données
    train_text = open("data/trump_full_speech.txt", "r").read()
    train_dataset = TextDataset(train_text)
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, collate_fn=pad_collate_fn)
    # test_dataset = TextDataset("data/test.txt", seq_length)
    # test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

    # Initialisation du modèle
    rnn = GRU(1, hidden_size, 1).to(device)
    rnn.init_hidden(batch_size)

    # Initialisation de l'optimiseur
    optimizer = torch.optim.Adam(rnn.parameters(), lr=learning_rate)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)

    # Initialisation du logger
    writer = SummaryWriter()

    # Boucle d'apprentissage
    for epoch in range(epochs):
        rnn.train()
        total_loss = 0
        for i, phrase in enumerate(train_loader):
            phrase = phrase.T
            # Préparer les données d'entrée et les cibles
            data = phrase[:, :20].to(device)  # Tous les caractères sauf le dernier
            targets = phrase[:, 21].to(device)  # Tous les caractères sauf le premier

            optimizer.zero_grad()
            rnn.init_hidden(batch_size)  # Réinitialise l'état caché pour chaque nouvelle phrase
            output = rnn(data)[:,-1,:]
            output_normalized = normalization(output)

                
            targets_emb = rnn.embed(targets)
            normalize_targets = normalization(targets_emb)
            # print(output[0], targets_emb[0])
            loss = maskedCrossEntropy(output_normalized, targets, normalize_targets, PAD_IX)
            loss.backward()
            optimizer.step()

            total_loss += loss.item()
            # convert embedding to index
        lr_scheduler.step()   

        avg_loss = total_loss / len(train_loader)
        print(f"Epoch {epoch+1}/{epochs}, Loss moyenne: {avg_loss}")


