from utils import SimpleRNN,RNN, device,SampleMetroDataset
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

# Nombre de stations utilisé
CLASSES = 10
#Longueur des séquences
LENGTH = 20
# Dimension de l'entrée (1 (in) ou 2 (in/out))
DIM_INPUT = 2
#Taille du batch
BATCH_SIZE = 32

PATH = "data/"


matrix_train, matrix_test = torch.load(open(PATH+"hzdataset.pch","rb"))
ds_train = SampleMetroDataset(matrix_train[:, :, :CLASSES, :DIM_INPUT], length=LENGTH)
ds_test = SampleMetroDataset(matrix_test[:, :, :CLASSES, :DIM_INPUT], length = LENGTH, stations_max = ds_train.stations_max)
data_train = DataLoader(ds_train,batch_size=BATCH_SIZE,shuffle=True)
data_test = DataLoader(ds_test, batch_size=BATCH_SIZE,shuffle=False)

#  TODO:  Question 2 : prédiction de la ville correspondant à une séquence
loss = torch.nn.CrossEntropyLoss()
def one_hot(y, classes):
    y_one_hot = torch.zeros(y.size(0), classes)
    y_one_hot.scatter_(1, y.view(-1, 1), 1)
    return y_one_hot.to(torch.long)

LATENT = 64
model = SimpleRNN(DIM_INPUT, LATENT, CLASSES)
optim = torch.optim.Adam(model.parameters(), lr=0.0001)
lr_scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size=5, gamma=0.1)


for i in tqdm(range(30)):
    loss_train = 0
    for data in data_train:
        x, y = data
        model.zero_grad()
        output = model(x)
        y_hat = model.decode(output)
        l = loss(y_hat[:,-1,:], y)
        l.backward()
        optim.step()
        loss_train += l.item()
    lr_scheduler.step()


    precision = 0
    l = 0
    for data in data_test:
        x, y = data
        output = model(x)
        y_hat = model.decode(output)
        l += loss(y_hat[:,-1,:], y)
        precision += (y_hat[:,-1,:].argmax(dim=1) == y).sum()
    print("Epoch", i,"Loss train",loss_train/len(data_train), "Loss test", l.item()/len(data_test), "Precision", precision.item()/len(data_test.dataset))

    
