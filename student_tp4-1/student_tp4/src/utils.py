import torch
import torch.nn as nn
from torch.utils.data import Dataset

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

import torch
import torch.nn as nn



class SimpleRNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size= output_size
        
        # Define learnable parameters: weights and bias
        self.wi = nn.Parameter(torch.randn(input_size, hidden_size))
        self.wh = nn.Parameter(torch.randn(hidden_size, hidden_size))
        self.bh = nn.Parameter(torch.zeros(1, hidden_size))
        self.fc = nn.Linear(hidden_size, output_size)
        self.tanh = nn.Tanh()

        
    def forward(self, x):
        batch_size,seq_length, _ = x.size()
        h = torch.zeros(batch_size, self.hidden_size)  # Initialize hidden state
        
        outputs = []
        for t in range(seq_length):
            xi = x[:, t, :]  # Current input at time step t
            ht = torch.tanh(torch.mm(xi, self.wi) + torch.mm(h, self.wh) + self.bh)
            h = ht  # Update hidden state for the next step
            outputs.append(ht)
        
        return torch.stack(outputs, dim=1)
    
    def decode(self,h):
        y = self.fc(h)
        return y



class RNN(nn.Module):
    def __init__(self, dim_input, dim_hidden, dim_output):
        super(RNN).__init__()
        self.dim_input = dim_input
        self.dim_hidden = dim_hidden
        self.dim_output = dim_output
        self.rnn = nn.RNN(dim_input, dim_hidden, batch_first=True)
        self.fc = nn.Linear(dim_hidden, dim_output)
        self.hidden = None

    def init_hidden(self, batch_size):
        # Initialize the hidden state, should be called at the beginning of each sequence
        self.hidden = torch.zeros(1, batch_size, self.dim_hidden)

    def many_to_many_forward(self, x):
        # Initialize hidden state
        self.init_hidden(x.size(0))

        out, self.hidden = self.rnn(x, self.hidden)
        return out

    def many_to_one_forward(self, x):
        # Initialize hidden state
        self.init_hidden(x.size(0))

        _, self.hidden = self.rnn(x, self.hidden)
        y = self.fc(self.hidden[:,-1,:])
        return y

    def many_to_many_variant(self, x):

        y=self.many_to_one_forward(self, x)
        # Generate outputs for i >= T using y1 and h_{i-1}
        outputs = [y]

        for _ in range(1,x.size(1)):
            # Generate outputs using the updated hidden state
            out, self.hidden = self.rnn(outputs[-1], self.hidden)
            outputs.append(out)
        
        return torch.cat(outputs, dim=1)



class SampleMetroDataset(Dataset):
    def __init__(self, data,length=20,stations_max=None):
        """
            * data : tenseur des données au format  Nb_days x Nb_slots x Nb_Stations x {In,Out}
            * length : longueur des séquences d'exemple
            * stations_max : normalisation à appliquer
        """
        self.data, self.length= data, length
        ## Si pas de normalisation passée en entrée, calcul du max du flux entrant/sortant
        self.stations_max = stations_max if stations_max is not None else torch.max(self.data.view(-1,self.data.size(2),self.data.size(3)),0)[0]
        ## Normalisation des données
        self.data = self.data / self.stations_max
        self.nb_days, self.nb_timeslots, self.classes = self.data.size(0), self.data.size(1), self.data.size(2)

    def __len__(self):
        ## longueur en fonction de la longueur considérée des séquences
        return self.classes*self.nb_days*(self.nb_timeslots - self.length)

    def __getitem__(self,i):
        ## transformation de l'index 1d vers une indexation 3d
        ## renvoie une séquence de longueur length et l'id de la station.
        station = i // ((self.nb_timeslots-self.length) * self.nb_days)
        i = i % ((self.nb_timeslots-self.length) * self.nb_days)
        timeslot = i // self.nb_days
        day = i % self.nb_days
        return self.data[day,timeslot:(timeslot+self.length),station],station

class ForecastMetroDataset(Dataset):
    def __init__(self, data,length=20,stations_max=None):
        """
            * data : tenseur des données au format  Nb_days x Nb_slots x Nb_Stations x {In,Out}
            * length : longueur des séquences d'exemple
            * stations_max : normalisation à appliquer
        """
        self.data, self.length= data,length
        ## Si pas de normalisation passée en entrée, calcul du max du flux entrant/sortant
        self.stations_max = stations_max if stations_max is not None else torch.max(self.data.view(-1,self.data.size(2),self.data.size(3)),0)[0]
        ## Normalisation des données
        self.data = self.data / self.stations_max
        self.nb_days, self.nb_timeslots, self.classes = self.data.size(0), self.data.size(1), self.data.size(2)

    def __len__(self):
        ## longueur en fonction de la longueur considérée des séquences
        return self.nb_days*(self.nb_timeslots - self.length)

    def __getitem__(self,i):
        ## Transformation de l'indexation 1d vers indexation 2d
        ## renvoie x[d,t:t+length-1,:,:], x[d,t+1:t+length,:,:]
        timeslot = i // self.nb_days
        day = i % self.nb_days
        return self.data[day,timeslot:(timeslot+self.length-1)],self.data[day,(timeslot+1):(timeslot+self.length)]

