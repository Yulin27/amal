from pathlib import Path
import os
from typing import Any
import torch
from tqdm import tqdm

from torchvision.utils import make_grid
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import datetime

# Téléchargement des données

from datamaestro import prepare_dataset

ds = prepare_dataset("com.lecun.mnist")
train_images, train_labels = ds.train.images.data(), ds.train.labels.data()
test_images, test_labels = ds.test.images.data(), ds.test.labels.data()

writer = SummaryWriter("runs/runs" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

BATCH_SIZE = 32
ITERATIONS = 10

class MonDataset(Dataset):
    def __init__(self, X, Y) -> None:
        super().__init__()
        self.X = X.reshape(-1, 784).astype(np.float32) / 255
        self.Y = Y

    def __getitem__(self, index):
        return self.X[index], self.Y[index]
    
    def __len__(self):
        return len(self.X)
    

class AutoEncoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size) -> None:
        super().__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
        )
        self.decoder = nn.Sequential(
            nn.Linear(hidden_size,output_size),
            nn.Sigmoid(),
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
    

class State:
    def __init__(self, model, optimizer) -> None:
        self.model = model
        self.optim = optimizer
        self.epoch = 0
        self.iteration = 0


# permet de selectionner le gpu si disponible
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

criterion = nn.MSELoss()

train_loader = DataLoader(MonDataset(train_images, train_labels), shuffle=True, batch_size=BATCH_SIZE)
test_loader = DataLoader(MonDataset(test_images, test_labels), shuffle=True, batch_size=BATCH_SIZE)


encoder = AutoEncoder(784, 128, 784)
encoder = encoder.to(device)


save_path = Path("model.pth")

if save_path.is_file():
    with save_path.open("rb") as fp:
        state = torch.load(fp)
else:
    state = State(encoder, torch.optim.Adam(encoder.parameters(), lr=0.001))

for epoch in tqdm(range(state.epoch, ITERATIONS)):

    epoch_loss = 0
    epoch_loss_test = 0


    for x,y in train_loader:
        state.optim.zero_grad()

        x = x.to(device)
        xhat = state.model(x)
        loss = criterion(xhat,x)
        epoch_loss += loss.sum()


        loss.backward()
        state.optim.step()
        state.iteration += 1
        
        writer.add_scalar('Loss/train', epoch_loss/len(x), epoch)

    with torch.no_grad():
        for x,y in test_loader:
            x = x.to(device)
            xhat = state.model(x)
            loss = criterion(xhat,x)
            epoch_loss_test += loss.sum()

            writer.add_scalar('Loss/test', epoch_loss_test/len(x), epoch)
            break

    with save_path.open("wb") as fp:
        state.epoch += 1
        torch.save(state, fp)


writer.close()