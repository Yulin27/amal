import logging
import re
from pathlib import Path
from tqdm import tqdm
import numpy as np

from datamaestro import prepare_dataset
import torch.nn.functional as F
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence


class FolderText(Dataset):
    """Dataset basé sur des dossiers (un par classe) et fichiers"""

    def __init__(self, classes, folder: Path, tokenizer, load=False):
        self.tokenizer = tokenizer
        self.files = []
        self.filelabels = []
        self.labels = {}
        for ix, key in enumerate(classes):
            self.labels[key] = ix

        for label in classes:
            for file in (folder / label).glob("*.txt"):
                self.files.append(file.read_text() if load else file)
                self.filelabels.append(self.labels[label])

    def __len__(self):
        return len(self.filelabels)

    def __getitem__(self, ix):
        s = self.files[ix]
        return self.tokenizer(s if isinstance(s, str) else s.read_text()), self.filelabels[ix]
    
def get_imdb_data(embedding_size=50):
    """Renvoie l'ensemble des donnéees nécessaires pour l'apprentissage

    - dictionnaire word vers ID
    - embeddings (Glove)
    - DataSet (FolderText)

    """
    WORDS = re.compile(r"\S+")

    words, embeddings = prepare_dataset('edu.stanford.glove.6b.%d' % embedding_size).load()
    OOVID = len(words)
    words.append("__OOV__")

    word2id = {word: ix for ix, word in enumerate(words)}
    embeddings = np.vstack((embeddings, np.zeros(embedding_size)))

    def tokenizer(t):
        return [word2id.get(x, OOVID) for x in re.findall(WORDS, t.lower())]

    logging.info("Loading embeddings")

    logging.info("Get the IMDB dataset")
    ds = prepare_dataset("edu.stanford.aclimdb")

    return word2id, embeddings, FolderText(ds.train.classes, ds.train.path, tokenizer, load=False), FolderText(ds.test.classes, ds.test.path, tokenizer, load=False)


#  TODO: 
BATCH_SIZE = 32
word2id, embeddings, train, test = get_imdb_data()
embeddings = torch.tensor(embeddings)

def collate_fn_padding(batch):
    batch_x, batch_y = zip(*batch)
    max_len = max([len(x) for x in batch_x])
    batch_x_padded = [[x+[0]*(max_len-len(x))] for x in batch_x]
    batch_x_padded = torch.tensor(batch_x_padded, dtype=torch.long)
    return batch_x_padded, torch.tensor(batch_y)


train_loader = DataLoader(train, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn_padding)
test_loader = DataLoader(test, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn_padding)

class MLP(nn.Module):
    def __init__(self, embeddings):
        super().__init__()
        self.embedding = nn.Embedding.from_pretrained(embeddings)
        self.fc1 = nn.Linear(50, 25)
        self.fc2 = nn.Linear(25, 2)
        self.query = nn.Parameter(torch.randn(50, requires_grad=True))
    
    def forward(self, x):
        x = self.embedding(x)
        x = x.float()
        attention_weights = F.softmax(torch.matmul(x, self.query), dim=1)
        x = torch.sum(x, torch.mul(x, attention_weights), dim=1)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
    



def train(model, train_loader, optimizer, loss):
    model.train()
    losses = 0.
    for x, y in tqdm(train_loader):
        x = x.squeeze()
        optimizer.zero_grad()
        y_pred = model(x)
        l = loss(y_pred, y)
        losses += l.item()
        l.backward()
        optimizer.step()
    return losses / len(train_loader)

def test(model, test_loader, loss):
    model.eval()
    losses = 0.
    with torch.no_grad():
        for x, y in tqdm(test_loader):
            x = x.squeeze()
            y_pred = model(x)
            l = loss(y_pred, y)
            losses += l.item()
    return losses/len(test_loader)


model = MLP(embeddings)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
loss = nn.CrossEntropyLoss()
for i in range(10):
    l_train = train(model, train_loader, optimizer, loss)
    l_test = test(model, test_loader, loss)
    print("Epoch %d: train loss: %f, test loss: %f" % (i, l_train, l_test))
