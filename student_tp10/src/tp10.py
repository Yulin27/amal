
import math
import click
from torch.utils.tensorboard import SummaryWriter
import logging
import re
from pathlib import Path
from tqdm import tqdm
import numpy as np
import time
from datamaestro import prepare_dataset
import torch.nn.functional as F
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from utils import PositionalEncoding



MAX_LENGTH = 500

logging.basicConfig(level=logging.INFO)

class FolderText(Dataset):
    """Dataset basé sur des dossiers (un par classe) et fichiers"""

    def __init__(self, classes, folder: Path, tokenizer, load=False):
        self.tokenizer = tokenizer
        self.files = []
        self.filelabels = []
        self.labels = {}
        for ix, key in enumerate(classes):
            self.labels[key] = ix

        for label in classes:
            for file in (folder / label).glob("*.txt"):
                self.files.append(file.read_text() if load else file)
                self.filelabels.append(self.labels[label])

    def __len__(self):
        return len(self.filelabels)

    def __getitem__(self, ix):
        s = self.files[ix]
        return self.tokenizer(s if isinstance(s, str) else s.read_text()), self.filelabels[ix]
    def get_txt(self,ix):
        s = self.files[ix]
        return s if isinstance(s,str) else s.read_text(), self.filelabels[ix]

def get_imdb_data(embedding_size=50):
    """Renvoie l'ensemble des donnéees nécessaires pour l'apprentissage (embedding_size = [50,100,200,300])

    - dictionnaire word vers ID
    - embeddings (Glove)
    - DataSet (FolderText) train
    - DataSet (FolderText) test

    """
    WORDS = re.compile(r"\S+")

    words, embeddings = prepare_dataset(
        'edu.stanford.glove.6b.%d' % embedding_size).load()
    OOVID = len(words)
    words.append("__OOV__")
    word2id = {word: ix for ix, word in enumerate(words)}
    embeddings = np.vstack((embeddings, np.zeros(embedding_size)))

    def tokenizer(t):
        return [word2id.get(x, OOVID) for x in re.findall(WORDS, t.lower())]

    logging.info("Loading embeddings")

    logging.info("Get the IMDB dataset")
    ds = prepare_dataset("edu.stanford.aclimdb")

    return word2id, embeddings, FolderText(ds.train.classes, ds.train.path, tokenizer, load=False), FolderText(ds.test.classes, ds.test.path, tokenizer, load=False)

#  TODO: 

class AttentionModule(nn.Module):
    def __init__(self, embedding_size):
        super().__init__()
        self.embedding_size = embedding_size
        self.query = nn.Linear(embedding_size, embedding_size)
        self.key = nn.Linear(embedding_size, embedding_size)
        self.value = nn.Linear(embedding_size, embedding_size)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x, lens):
        q = self.query(x)
        k = self.key(x)
        v = self.value(x)
        attn = torch.bmm(q, k.transpose(1, 2))
        attn = attn.masked_fill(attn == 0, -1e9)
        attn = self.softmax(attn)
        attn = attn.masked_fill(attn == 0, 0)
        x = torch.bmm(attn, v)
        x = x.squeeze(1)
        return x




class base(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(50, 25)
        self.fc2 = nn.Linear(25, 2)
    
    def forward(self, x):
        x = x.float()
        x = torch.sum(x, dim=1)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
    
# class attention using AttentionModule
class Attention1(nn.Module):
    def __init__(self, emb_size):
        super().__init__()
        self.attention1 = AttentionModule(emb_size)
        self.attention2 = AttentionModule(emb_size)
        self.attention3 = AttentionModule(emb_size)
        self.fc1 = nn.Linear(emb_size, 25)
        self.fc2 = nn.Linear(25, 2)
        self.relu = nn.ReLU()

    def forward(self, x, lens):
        x = x.float()
        x = self.attention1(x, lens)
        x = self.relu(x)
        x = self.attention2(x, lens)
        x = self.relu(x)
        x = self.attention3(x, lens)
        x = self.relu(x)
        x = torch.mean(x, dim=1)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
    

# class attention using residual connection
class Attention2(nn.Module):
    def __init__(self, emb_size):
        super().__init__()
        self.attention1 = AttentionModule(emb_size)
        self.attention2 = AttentionModule(emb_size)
        self.attention3 = AttentionModule(emb_size)
        self.fc1 = nn.Linear(emb_size, 25)
        self.fc2 = nn.Linear(25, 2)
        self.relu = nn.ReLU()

    def forward(self, x, lens):
        x = x.float()
        x = self.attention1(x, lens)+x
        x = self.relu(x)
        x = self.attention2(x, lens)+x
        x = self.relu(x)
        x = self.attention3(x, lens)+x
        x = self.relu(x)
        x = torch.mean(x, dim=1)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
    
# class attention using positional encoding
class Attention3(nn.Module):
    def __init__(self, emb_size):
        super().__init__()
        self.attention1 = AttentionModule(emb_size)
        self.fc1 = nn.Linear(emb_size, 25)
        self.fc2 = nn.Linear(25, 2)
        self.relu = nn.ReLU()
        self.pe = PositionalEncoding(emb_size)

    def forward(self, x, lens):
        x = x.float()
        x = self.pe(x)
        x = self.attention1(x, lens)
        x = self.relu(x)

        x = torch.mean(x, dim=1)
        x = self.fc1(x)
        x = self.fc2(x)
        return x
        

@click.command()
@click.option('--test-iterations', default=1000, type=int, help='Number of training iterations (batches) before testing')
@click.option('--epochs', default=50, help='Number of epochs.')
@click.option('--modeltype', default=1, type=int, help="0: base, 1 : Attention1, 2: Attention2")
@click.option('--emb-size', default=50, help='embeddings size')
@click.option('--batch-size', default=20, help='batch size')

def main(epochs,test_iterations,modeltype,emb_size,batch_size):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    word2id, embeddings, train_data, test_data = get_imdb_data(emb_size)
    id2word = dict((v, k) for k, v in word2id.items())
    PAD = word2id["__OOV__"]
    embeddings = torch.Tensor(embeddings)
    emb_layer = nn.Embedding.from_pretrained(torch.Tensor(embeddings))

    def collate(batch):
        """ Collate function for DataLoader """
        data = [torch.LongTensor(item[0][:MAX_LENGTH]) for item in batch]
        lens = [len(d) for d in data]
        labels = [item[1] for item in batch]
        return emb_layer(torch.nn.utils.rnn.pad_sequence(data, batch_first=True,padding_value = PAD)).to(device), torch.LongTensor(labels).to(device), torch.Tensor(lens).to(device)


    train_loader = DataLoader(train_data, shuffle=True,
                          batch_size=batch_size, collate_fn=collate)
    test_loader = DataLoader(test_data, batch_size=batch_size,collate_fn=collate,shuffle=False)

    model = Attention1(emb_size).to(device) if modeltype == 1 else base(embeddings).to(device)
    optimizer = torch.optim.Adam(model.parameters())    
    loss = nn.CrossEntropyLoss()
    writer = SummaryWriter()

    for epoch in range(epochs):
        model.train()
        losses = 0.
        for x, y, lens in tqdm(train_loader):
            optimizer.zero_grad()
            y_pred = model(x, lens)
            l = loss(y_pred, y)
            losses += l.item()
            l.backward()
            optimizer.step()
        writer.add_scalar("train_loss", losses / len(train_loader), epoch)
        print("Epoch %d: train loss: %f" % (epoch, losses / len(train_loader)))
        model.eval()
        losses = 0.
        with torch.no_grad():
            for x, y, lens in tqdm(test_loader):
                y_pred = model(x, lens)
                l = loss(y_pred, y)
                losses += l.item()
        writer.add_scalar("test_loss", losses / len(test_loader), epoch)
        print("Epoch %d:  test loss: %f" %
              (epoch, losses / len(test_loader)))

    writer.close()




if __name__ == "__main__":
    main()

