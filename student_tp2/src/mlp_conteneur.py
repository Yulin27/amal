import torch
from torch.utils.tensorboard import SummaryWriter
## Installer datamaestro et datamaestro-ml pip install datamaestro datamaestro-ml
import datamaestro
from tqdm import tqdm

writer = SummaryWriter()

data = datamaestro.prepare_dataset("edu.uci.boston")
colnames, datax, datay = data.data()

# Normaliser les données
datax_mean = datax.mean(axis=0)
datax_std = datax.std(axis=0)
datax = (datax - datax_mean) / datax_std

datax = torch.tensor(datax,dtype=torch.float,requires_grad=True)
datay = torch.tensor(datay,dtype=torch.float).reshape(-1,1)


# TODO: 
input_size = datax.shape[1]
output_size = datay.shape[1]
hidden_size = 6

epsilon = 0.01
nb_epochs = 100

modele = torch.nn.Sequential(
    torch.nn.Linear(input_size, hidden_size),
    torch.nn.Tanh(),
    torch.nn.Linear(hidden_size, output_size),
)

loss_fn = torch.nn.MSELoss(reduction='mean')

optim = torch.optim.SGD(modele.parameters(), lr=epsilon)
optim.zero_grad()

for epoch in tqdm(range(nb_epochs)):
    
    yhat = modele(datax)
    loss = loss_fn(yhat, datay)
    loss.backward()

    optim.step()
    optim.zero_grad()

    print(f"Itérations {epoch}: loss {loss}")

    writer.add_scalar('Loss/train', loss, epoch)


writer.close()


