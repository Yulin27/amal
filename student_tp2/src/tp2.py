import torch
from torch.utils.tensorboard import SummaryWriter
## Installer datamaestro et datamaestro-ml pip install datamaestro datamaestro-ml
import datamaestro
from tqdm import tqdm

writer = SummaryWriter()

data = datamaestro.prepare_dataset("edu.uci.boston")
colnames, datax, datay = data.data()

# Normaliser les données
datax_mean = datax.mean(axis=0)
datax_std = datax.std(axis=0)
datax = (datax - datax_mean) / datax_std

datax = torch.tensor(datax,dtype=torch.float,requires_grad=True)
datay = torch.tensor(datay,dtype=torch.float).reshape(-1,1)


# TODO: 
input_size = datax.shape[1]
output_size = datay.shape[1]

epsilon = 0.01
nb_epochs = 100

w = torch.randn(input_size, output_size, requires_grad=True)
b = torch.randn(output_size, requires_grad=True)

def descent_grad_batch(datax, datay, w, b, epsilon, nb_epochs):

    for epoch in tqdm(range(nb_epochs)):
        
        yhat = torch.mm(datax, w) + b
        loss = torch.mean((yhat - datay) ** 2)
        loss.backward()


        with torch.no_grad():
            w -= epsilon * w.grad
            b -= epsilon * b.grad

        w.grad.zero_()
        b.grad.zero_()

        print(f"Itérations {epoch}: loss {loss}")

        writer.add_scalar('Loss/train', loss, epoch)

def descent_grad_stoc(datax, datay, w, b, epsilon, nb_epochs):
    for epoch in tqdm(range(nb_epochs)):

        for i in range(len(datax)):

            x_sample = datax[i].reshape(1,-1)
            y_sample = datay[i].reshape(1,-1)

            yhat = torch.mm(x_sample, w) + b
            loss = torch.mean((yhat - y_sample) ** 2)

            loss.backward()


            with torch.no_grad():
                w -= epsilon * w.grad
                b -= epsilon * b.grad

            w.grad.zero_()
            b.grad.zero_()

        print(f"Itérations {epoch}: loss {loss}")
        writer.add_scalar('Loss/train', loss, epoch)

def descent_grad_mini_batch(datax, datay, w, b, epsilon, batch_size, nb_epochs):
    for epoch in tqdm(range(nb_epochs)):
        for i in range(0, len(datax), batch_size):

            x_batch = datax[i:i+batch_size]
            y_batch = datay[i:i+batch_size]


            yhat = torch.mm(x_batch, w) + b
            loss = torch.mean((yhat - y_batch) ** 2)

            loss.backward()


            with torch.no_grad():
                w -= epsilon * w.grad
                b -= epsilon * b.grad

            w.grad.zero_()
            b.grad.zero_()

        print(f"Itérations {epoch}: loss {loss}")
        writer.add_scalar('Loss/train', loss, epoch)


descent_grad_batch(datax, datay, w, b, epsilon, nb_epochs)
# descent_grad_stoc(datax, datay, w, b, epsilon, nb_epochs)
# descent_grad_mini_batch(datax, datay, w, b, epsilon, 32, nb_epochs)
writer.close()


